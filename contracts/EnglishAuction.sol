pragma solidity ^0.4.24;

import "./Auction.sol";

contract EnglishAuction is Auction {

    uint internal highestBid;
    uint internal initialPrice;
    uint internal biddingPeriod;
    uint internal lastBidTimestamp;
    uint internal minimumPriceIncrement;

    bool firstBidPlaced;

    address internal highestBidder;

    constructor(
        address _sellerAddress,
        address _judgeAddress,
        Timer _timer,
        uint _initialPrice,
        uint _biddingPeriod,
        uint _minimumPriceIncrement
    ) public Auction(_sellerAddress, _judgeAddress, _timer) {
        initialPrice = _initialPrice;
        highestBid = initialPrice;
        biddingPeriod = _biddingPeriod;
        minimumPriceIncrement = _minimumPriceIncrement;
        lastBidTimestamp = time();
        highestBidder = address(0);
        firstBidPlaced = false;
    }

    function bid() public payable {
        require(outcome == Outcome.NOT_FINISHED && time() < lastBidTimestamp + biddingPeriod);
        uint nextPrice = highestBid;
        if (firstBidPlaced == true) {
            nextPrice += minimumPriceIncrement;
        }
        if (msg.value >= nextPrice) {
            uint return_amount = 0;
            address previous_highest_bidder = address(0);
            if (firstBidPlaced == true) {
                return_amount = highestBid;
                previous_highest_bidder = highestBidder;
            }

            highestBidder = msg.sender;
            lastBidTimestamp = time();
            highestBid = msg.value;
            firstBidPlaced = true;

            if(return_amount != 0 && previous_highest_bidder != address(0)) {
                previous_highest_bidder.transfer(return_amount);
            }

        } else {
            msg.sender.transfer(msg.value);
            require(false);
        }
    }

    function getHighestBidder() public returns (address) {
        if (time() < lastBidTimestamp + biddingPeriod) {
            return address(0);
        }
        return highestBidder;
    }
}
